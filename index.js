// console.log("TGIF!");

// What are conditional statements?

// Conditional statements allow us to control the flow of our program
// It allows us to run statements/instructions if a condition is met or run another separate instructions if otherwise.

// [Section] if, else if and else statement

let numA = -1;

/*
	if Statement
		it will execute the satetement if a specified condition is met/true
*/

if(numA<0){
	console.log("Hello");
}

console.log(numA<0);

/*
	Syntax:
	if(conditin){
		statement;
	}
*/

// The result of the expression added in the if's condition must result to true, else, the statement inside if() will not run.
// lets update the variable and run an if statement  with the same condition:

numA = 0;

if(numA<0){
	console.log("Hello again if numA is 0!");
}

console.log(numA<0);

// it will not run because the expression now results to false.

let city = "New York";

if(city === "New York"){
	console.log("Welcome tp New York");
}

// else if clause
/*
	-Execute a statment if previous conditions are false and if the specified condition is trie
	-The "else if" clause is optional and can be added to capture additional conditions to chagen the flow of th program
*/

let numH = 1;

if(numH < 0){
	console.log("Hellow form numH");
}
else if(numH > 0){
	console.log("Hi I'm H!");
}

// We were able to run the else if() statement after  we evaluateed that the if statement was false.

// If the if() condition was passed and run, we will no longer evaluate to else if() and end the process there.

if(numH < 0){
	console.log("Hellow form numH");
}
else if(numH === 1){
	console.log("Hi I'm the second condition met!");
}
else if(numH<0){
	console.log("Hi I'm H!");
}

console.log(numH>1);
console.log(numH === 1);
console.log(numH<0);

// else if() statement was not executed because the if statement was able to run, the evaluation of the whole statements stops there.

// Let's update the city variable and look at another example:

city = "Tokyo";

if(city === "New York"){
	console.log("Welcome to New York!");
}
else if(city === "Tokyo"){
	console.log("Welcome to Tokyo, Japan!");
}

// else statement
/*
	-Executes a statement if all other conditions are fals / not met
	-else statements are optional and can be added to capture any other result to change the flow of a program.
*/

numH = 2;

if(numH<0){
	console.log("Hello I'm numH");
}
else if(numH>2){
	console.log("numH is greater than 2");
}
else if(numH>3){
	console.log("numH is greater than 3");
}
else{
	console.log("numH from else");
}

/*
	since all of the preceding if and else if conditions failed, the else statement was run instead.

	else and else if statements should only be added if there is a preceeding if conditions, else, statements by itself will not work,
	however if statements will work even if there is no else statements.

*/

/*
{
	else{
		console.log("Will not run without an if");
	}
}
	It will result into an error.
*/

/*
{
	let numB = 1;
	else if(numB === 1){
		console.log("numB===1");
	}
	Same goes for and else if, there should be a preceeding if().
}
*/

	// if, else if and else statements with functions

	/*
		-Most of the time we would like to use if, else if, and else statements with functions to control the flow of your application.
		-By including them inside the function, we can decide when certain conditions will be checked instead of executing statements when the JavaScript loads
		-The "return" statement can utilize with conditional statements in combination with function to change values to be used for other features of our application		
	*/

let message;

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed<31){
		return "Not a typhoon yet.";
	}
	else if(windSpeed <= 60){
		return "Tropical Depression Detected.";
	}
	else if(windSpeed >=  61 && windSpeed <= 88){
		return "Tropical Storm Detected.";
	}
	else{
		return "Typhoon Detected.";
	}
}
	// Returns the string to the variable message taht invoked.
	message = determineTyphoonIntensity(123);
	console.log(message);

	/*
		-We can further control the flow of our program based on conditions and changing vairables and results.
		-Due to the conditional statements created in the situation, we were able to reassing it's value and us it's new value to print different output.
		-console.warn() is a good way to pring warning in our console tat could help us developers act on certain output within our code.		
	*/

	if(message === "Typhoon Detected."){
		console.warn(message);
	}

// [Section] Truthy and Falsy.

	/*
		In JavaScript a "Thruthy" value is a vlue that is considered true when encountered in a Boolean context
		Value are considered true unless defined otherwise.
		Fasly values/exceptions for truthy:
		1. false
        2. 0
        3. -0
        4. ""
        5. null
        6. undefined
        7. NaN - Not a Number
	*/

// Truthy Examples

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}

// Falsy Examples

if(false){
	console.log("Falsy");
}

if(0){
	console.log("Falsy");
}

// Single Statement Execution
let ternaryResults = (1 < 18) 1 : 2;
console.log("Result of Ternary Operator: " ternaryResults);

// Multiple statement execution
// Both functions perform two separate tasks which changes the value of the 

let name;

function isOfLegalAge(){
	name = 'John';
	return "You are of the legal age!";
}

function isUnderAge(){
	name = 'Jane';
	return "You are under age limit";
}

//The parseInt() function converts 
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age >= 18 ) ? isOfLegalAge() : isUnderAge();

console.log("Results of Ternary Operator in Functions: " + legalAge + ", " + name);

// [Section] Switch statement

/*
	The switch statement evaluates an expression and matches the expression's value to a case claus.
*/

/*

Syntax:

switch (expression){
	case value:
		statement;
		break;
	default:
		statement;
		break;
}

*/

let day = prompt("What day of the week is it today? ").toLowerCase();
console.log(day);

switch(day){
	case 'monday':
		console.log("The color of the day is red!");
		break;
	case 'tuesday':
		console.log("The color of the day is orange!");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow!");
		break;
	case 'thursday':
		console.log("The color of the day is green!");
		break;
	case 'friday':
		console.log("The color of the day is blue!");
	case "saturday":
		console.log("The color of the day is indigo!");
	case "sunday":
		console.log("The color of the day is violet!");
}